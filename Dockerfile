FROM python:3.6-alpine

RUN apk add --no-cache gcc musl-dev libffi-dev libressl-dev
RUN pip install --no-cache-dir cryptography
RUN apk del gcc musl-dev libffi-dev libressl-dev

RUN python -m pip install twine
