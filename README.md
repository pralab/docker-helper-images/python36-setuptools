# Python 3.6 with setuptools

Docker image based on `python:3.6-alpine` official docker image with setuptools for building releasable Python packages.

Compared to the standard `python:3.6-alpine` image, the following packages are available:
 - `twine`, utility for publishing Python packages on PyPI

## Usage

### As Gitlab CI job image
Set this docker image as the image for any Gitlab CI job as follows:
```yaml
job1:
  - image: $CI_REGISTRY/pralab/docker-helper-images/python36-setuptools:latest
  - script:
    - command1
    - command2
```

### As a docker image directly
Use the docker image to run a Python command inside it. For example, navigate to a folder which contains a `setup.py` file and run:
- `docker run -v "$PWD":$PWD -w $PWD $CI_REGISTRY/pralab/docker-helper-images/python36-setuptools:latest python setup.py sdist`

To access the container registry where the docker image is stored follow the instructions provided in the **Packages -> Container Registry** menu of this repository.
